const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require ("../auth");


// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Allows us to export the "router" object that will be accessed in "index.js"\
module.exports = router;


// Routes for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(
		resultFromController));
})

// Route for setting a user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	userController.updateUser(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})

/*--------------------------*/




// Allows us to export the "router" object that will be accessed in "index.js"\
module.exports = router;
