const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// USER REGISTRATION
module.exports.registerUser = (reqBody) => {
	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return ("Registration Successful!");
		}
	})
}

// USER AUTHENTICATION
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then ( result => {
		if(result == null){
			return ("Incorrect email or password! Please try again.");
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}
			else{
				return ("Incorrect email or password! Please try again.");
			}
		}
	})
}

// SET USER TO ADMIN
module.exports.updateUser = async (user, reqParams, reqBody) => {

	if(user.isAdmin){	
		let updatedUser = {
			isAdmin : reqBody.isAdmin	
			
		}

		return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {
			if(error){
				return false;
			}
			else{
				return ('Setting other user to admin is successful!');
			}
		})
	}else{
		return (`You have no access`);
	}
}
// ---------------

// CREATE ORDER
//  Async await will be used in enrolling the user because we will need to update two separate documents when enrolling a user
module.exports.checkout = async (userData,data) => {

	if(userData.isAdmin){


	// Add the product ID in the orders array of the user
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Adds the productId in the user's orders array
		user.orders.push({productId: data.productId});

		// Saves the updated information in the database
		return user.save().then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	// Add the user ID and product ID in the orderDetails array of the order 
	let isOrderUpdated =  await Order.findById(data.userId).then(order => {
		// Add the userId in the product's orderDetails array
		order.orderDetails.push({userId: data.userId});

		// Save the update course information in the database
		return order.save().then((order, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	
	
	// Condition that will check if the user and course documents have been updatedddd
	if(isUserUpdated && isProductUpdated){
		return true;
	}
	else{
		return false;
	}
}


else{
	return(`You are not allowed to create an order`);
	}
}
